﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Home_Work_47.Models
{
    public class Car
    {
        public int Id { get; set; }
       public string ModelName { get; set; }
       public string MakeName { get; set; }
       public int VinCode {get; set; }
       public int Year {get; set; }
       public User User { get; set; }
    }
}
