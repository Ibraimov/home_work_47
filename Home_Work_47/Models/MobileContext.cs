﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Home_Work_47.Models
{
    public class MobileContext : DbContext
    {
        public DbSet<User> Phones { get; set; }
        public DbSet<Car> Orders { get; set; }
        public MobileContext(DbContextOptions<MobileContext> options)
            : base(options)

        {

        }
    }
}
